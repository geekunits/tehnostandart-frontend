$(function() {

	/***
	* header search
	***/

	$(".header-nav__search").on("click", function(e){
		e.preventDefault();
		$(".header-search").stop(true, true).fadeToggle();
	});

	$(".header-search__exit-btn").on("click", function(e){
		e.preventDefault();
		$(".header-search").stop(true, true).fadeOut();
	});

	/***
	* header burger menu
	***/

	$(".header-nav__burger, .header-burmenu__close").on("click", function(e){
		e.preventDefault();
		$(".header-burmenu").stop(true, true).fadeToggle();
	});

	$('body').on('click', function (e) {
		if (!$(".header-burmenu").is(e.target) && $(".header-burmenu").has(e.target).length === 0 && $(".header-nav").has(e.target).length === 0) {
			$('.header-burmenu').stop(true, true).fadeOut(300);
		};
	});
	
	/***
	* owl top main
	***/

	$(".owl-top-main").owlCarousel({
		nav:false,
		dots:true,
		margin:0,
		loop:true,
		items: 1,
		singleItem: true
	});

	/***
	* owl top main
	***/

	$(".owl-clients").owlCarousel({
		nav:true,
		dots:false,
		margin:0,
		loop:false,
		items: 1,
		margin: 22,
		responsive : {
			768 : {
				items: 3
			},

			992 : {
				items: 5
			},

			1200 : {
				items: 5
			}
		}
	});


	/***
	* change div file name attach file 
	***/

	$('#modal-question__file, #modal-big-question__file').on("change", function(e) {
		imgNameToDiv( $(e.target) ); 
	});

	var imgNameToDiv = function(_target) { 
		var indexForSlice = _target.val().lastIndexOf('\\');
		_target.siblings('.modal-question__file-name').html(_target.val().substring(indexForSlice+1));
	};

	/***
	* page news dropdown
	***/

	$('.dropdown-button').on('click', function (e) {
		e.preventDefault();
		$(this).toggleClass('open');
		$(this).parents('.dropdown').find('.dropdown-menu').first().stop(true, true).fadeToggle(300);
	});

	$('body').on('click', function (e) {
		if ( !$('.dropdown').is(e.target) && $('.dropdown').has(e.target).length === 0 && $('.open').has(e.target).length === 0 ) {
			$('.dropdown').find('.dropdown-menu').first().stop(true, true).fadeOut(300);
			$('.dropdown').find('.dropdown-button').removeClass('open');
		}
	});

	/***
	* funcybox
	***/

	$(".funcybox-link").fancybox();

	/*
	* select-2
	*/

	if ( $('.select').length ) {
		$('.select').select2({
			placeholder: 'Тематика запроса'
		});
	};

}());

